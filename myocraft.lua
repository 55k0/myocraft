scriptId = 'com.myacaseakei.myocraft.desktop'

--replace with logic for finding media player
activeApp = "Game"

-- Controls
BACK = "backspace"
SHIFT = "shift"
RIGHT_ARROW = "right_arrow"

centerMode = false

-- Callback Functions

function activeAppName()

	return activeApp

end

function onForegroundWindowChange(app, title)

	if app == "com.apple.javajdk16.cmd" then

	return true

	else
	
	return false
	
	end

end

function onActiveChange(isActive)

	if isActive then
	
		myo.debug("active")
	
	else
	
		myo.debug("inactive")
	
	end

	--enable locking system

end

function mousePoseControl(pose, edge)

	if pose == "fist" then
			
		if edge == "on" then
			
		myo.mouse("left", "down")
	
		else
	
		myo.mouse("left", "up")
	
		end
	
	end

	if pose == "fingersSpread" then
			
		if edge == "on" then
			
		myo.mouse("right", "down")
	
		else
	
		myo.mouse("right", "up")
	
		end
	
	end

	if pose == "thumbToPinky" then
	
		if edge == "on" then
	
			if centerMode then
		
				--centerMode = false
		
			else 
		
				--centerMode = true
		
			end
	
		end
	
	end

end

function onPoseEdge(pose, edge)

	--myo.debug("pose:" .. pose)
	--myo.debug("edge:" .. edge)
	
	mousePoseControl(pose, edge)
	
end

function onPeriodic()

	if centerMode then

	myo.centerMousePosition()

	end

end

--Var init

myo.controlMouse(true)